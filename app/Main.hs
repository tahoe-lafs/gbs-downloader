module Main where

import Control.Monad.Except (runExceptT)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C8
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T
import Data.Yaml (decodeEither')
import System.Environment (getArgs)
import Tahoe.Announcement (Announcements (..))
import Tahoe.CHK.Capability (CHK (CHKReader), pCapability)
import Tahoe.Download (announcementToImmutableStorageServer, download)
import Text.Megaparsec (parse)

main :: IO ()
main = do
    [announcementPath, readCap] <- getArgs
    -- Load server announcements
    announcementsBytes <- B.readFile announcementPath
    let Right (Announcements announcements) = decodeEither' announcementsBytes

    -- Accept & parse read capability
    let Right (CHKReader cap) = parse pCapability "<argv>" (T.pack readCap)

    -- Download & decode the shares
    result <- runExceptT $ download announcements cap announcementToImmutableStorageServer

    -- Show the result
    putStrLn "Your result:"
    either print (C8.putStrLn . BL.toStrict) result
