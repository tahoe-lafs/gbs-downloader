{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module Main where

import Control.Monad.Except (ExceptT (ExceptT), MonadTrans (lift), runExceptT, withExceptT)
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.Text as T
import Data.Yaml (decodeFileEither)
import System.Environment (getArgs)
import Tahoe.Announcement (Announcements (..))
import Tahoe.Download (announcementToMutableStorageServer, download)
import Tahoe.SDMF (Reader, SDMF (..), pCapability, writerReader)
import Text.Megaparsec (parse)

newtype Failed = Failed String

instance Show Failed where
    show (Failed s) = s

main :: IO ()
main =
    either (putStrLn . ("Failed: " <>) . show) (BL.putStrLn . BL.append "Your result: ")
        =<< ( runExceptT $
                do
                    [announcementPath, readCap] <- lift getArgs

                    -- Load server announcements
                    announcements <- mightFail "Failed to parse announcements: " $ decodeFileEither announcementPath

                    -- Parse read capability
                    (mightFail "Failed to parse capability: " . pure . parse pCapability "<argv>" . T.pack) readCap
                        >>= \case
                            SDMFVerifier _ -> ExceptT . pure . Left . Failed $ "Nothing implemented for verifier capabilities."
                            SDMFWriter rwcap -> go announcements (writerReader rwcap)
                            SDMFReader rocap -> go announcements rocap
            )
  where
    go :: Announcements -> Reader -> ExceptT Failed IO BL.ByteString
    go (Announcements announcements) cap = withExceptT (Failed . ("Failed download: " <>) . show) $ download announcements cap announcementToMutableStorageServer

mightFail :: (Functor m, Show a1) => String -> m (Either a1 a2) -> ExceptT Failed m a2
mightFail s = withExceptT (Failed . (s <>) . show) . ExceptT
