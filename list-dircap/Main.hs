{-# LANGUAGE FlexibleContexts #-}

module Main where

import qualified Data.ByteString as B
import qualified Data.Text as T
import Data.Yaml (decodeEither')
import System.Environment (getArgs)
import Tahoe.Announcement (Announcements (..))
import qualified Tahoe.Directory as TD
import Text.Megaparsec (parse)

import Control.Monad.Except (runExceptT)
import Tahoe.Download (announcementToImmutableStorageServer, announcementToMutableStorageServer, downloadDirectory)

main :: IO ()
main = do
    [announcementPath, dirReadCap] <- getArgs
    -- Load server announcements
    announcementsBytes <- B.readFile announcementPath
    let Right (Announcements announcements) = decodeEither' announcementsBytes

    -- Accept & parse read capability
    case parse TD.pReadSDMF "<argv>" (T.pack dirReadCap) of
        Right r -> go announcements r announcementToMutableStorageServer
        Left eSDMF -> case parse TD.pReadCHK "<argv>" (T.pack dirReadCap) of
            Right r -> go announcements r announcementToImmutableStorageServer
            Left eCHK -> do
                print $ "Failed to parse cap: " <> show eSDMF
                print $ "Failed to parse cap: " <> show eCHK
  where
    go announcements cap lookupServer = do
        -- Download & decode the shares
        result <- runExceptT $ downloadDirectory announcements cap lookupServer

        -- Show the result
        putStrLn "Your result:"
        either print print result
