# GBS-Downloader

[![Most recent pipeline status badge](https://gitlab.com/tahoe-lafs/gbs-downloader/badges/main/pipeline.svg)](https://gitlab.com/tahoe-lafs/gbs-downloader/-/pipelines?scope=all&ref=main)

## What is it?

GBS-Downloader integrates Tahoe-CHK with Tahoe-Great-Black-Swamp to support downloading and decoding data from Great Black Swamp servers.
It aims for bit-for-bit compatibility with the original Python implementation.

### What is the current state?

* It can download immutable and mutable shares from Great Black Swamp storage servers.
  * It cryptographically verifies the identity of servers it communicates with.
* It can interpret, decode, and decrypt the data for CHK- and SDMF-encoded shares to recover the plaintext.

## Why does it exist?

A Haskell implementation can be used in places the original Python implementation cannot be
(for example, runtime environments where it is difficult to have a Python interpreter).
Additionally,
with the benefit of the experience gained from creating and maintaining the Python implementation,
a number of implementation decisions can be made differently to produce a more efficient, more flexible, simpler implementation and API.
Also,
the Python implementation claims no public library API for users outside of the Tahoe-LAFS project itself.
