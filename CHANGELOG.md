# Revision history for gbs-downloader

## 0.2.0.0 -- YYYY-MM-DD

* The download APIs now only send requests to a storage server after that
  storage server is authenticated using information from the NURL.

* ``Tahoe.Download.download`` and ``Tahoe.Download.downloadDirectory`` now return ``ExceptT``.

## 0.1.0.0 -- 2023-08-17

* First version. Released on an unsuspecting world.
* Basic support for loading storage service announcements for server discovery.
* Basic support for downloading the contents associated with a CHK or SDMF read capability.
  * CHK and SDMF directories are also supported.
