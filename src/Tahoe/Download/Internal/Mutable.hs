-- | Functionality related to retrieving "mutable" shares (for example, SDMF).
module Tahoe.Download.Internal.Mutable where

import Control.Monad.IO.Class (MonadIO)
import Tahoe.Announcement (StorageServerAnnouncement)
import Tahoe.CHK.Server (StorageServer)
import Tahoe.Download.Internal.Client (LookupError, announcementToStorageServer)
import TahoeLAFS.Storage.Client (getMutableShareNumbers, readMutableShares)

{- | Interpret the location in an announcement as a Tahoe-LAFS fURL pointed at a
 Great Black Swamp server and construct a StorageServer for interacting with
 mutable shares stored on it.
-}
announcementToMutableStorageServer :: MonadIO m => StorageServerAnnouncement -> m (Either LookupError StorageServer)
announcementToMutableStorageServer = announcementToStorageServer getMutableShareNumbers readMutableShares
