-- | Functionality related to retrieving "immutable" shares (mainly CHK).
module Tahoe.Download.Internal.Immutable where

import Control.Monad.IO.Class (MonadIO)
import Tahoe.Announcement (StorageServerAnnouncement)
import Tahoe.CHK.Server (StorageServer)
import Tahoe.Download.Internal.Client (LookupError, announcementToStorageServer)
import TahoeLAFS.Storage.Client (getImmutableShareNumbers, readImmutableShare)

{- | Interpret the location in an announcement as a Tahoe-LAFS fURL pointed at a
 Great Black Swamp server and construct a StorageServer for interacting with
 immutable shares stored on it.
-}
announcementToImmutableStorageServer :: MonadIO m => StorageServerAnnouncement -> m (Either LookupError StorageServer)
announcementToImmutableStorageServer = announcementToStorageServer getImmutableShareNumbers readImmutableShare
